build:
	docker-compose down
	docker-compose up -d --build
	mongoimport --db testdb --collection games --file ./mongo/dataDB.json --jsonArray
