const express = require('express');
const gamesController = require('../controllers/games-controller');
const router = new express.Router();

router.get('/get-top', gamesController.getGames);
router.get('/:id', gamesController.getOne);
router.post('/add-game', gamesController.addGame);
router.post('/update-game', gamesController.updateGame)
router.delete('/delete-games', gamesController.deleteGames)

module.exports = router;
