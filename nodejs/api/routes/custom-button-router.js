const express = require('express');
const customButtonController = require('../controllers/custom-button-controller');
const router = new express.Router();

router.get('/get-buttons', customButtonController.getButtons);
router.get('/:ids', customButtonController.getGamesUsingGetRequest);
router.post('/get-games-post', customButtonController.getGamesUsingPostRequest);

module.exports = router;
