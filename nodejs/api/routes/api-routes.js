const express = require('express');
const starWarsRouter = require('./star-wars-routes');
const gamesRouter = require('./games-routes');
const customButtonRouter = require('./custom-button-router');

const router = new express.Router();

router.use('/star-wars', starWarsRouter);
router.use('/games', gamesRouter);
router.use('/custom-button', customButtonRouter);

module.exports = router;
