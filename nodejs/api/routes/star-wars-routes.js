const express = require('express');
const starWarsController = require('../controllers/star-wars-controller');
const router = new express.Router();

router.get('/films', starWarsController.getFilms);
router.get('/planets', starWarsController.getPlanets);
router.get('/peoples', starWarsController.getPeople);

module.exports = router;
