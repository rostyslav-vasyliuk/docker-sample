const mongoose = require('mongoose');

const gamesSchema = new mongoose.Schema({
  title: { type: String },
  year: { type: String },
  rating: { type: String },
  genre: { type: String },
})

const Games = mongoose.model('Games', gamesSchema);

module.exports = { Games };
