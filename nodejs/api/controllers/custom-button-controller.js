const { Games } = require('../models/games-model');

const getButtons = async (req, res) => {
  try {
    const buttonsArray = [];
    // const linkTemplate = `http://localhost:8082/api/games`;
    buttonsArray.push({
      label: 'GET (This Tab)',
      requestType: 'GET',
      tab: 'this',
    });
    buttonsArray.push({
      label: 'GET (New Tab)',
      requestType: 'GET',
      tab: 'new',
    });
    buttonsArray.push({
      label: 'GET (AJAX)',
      requestType: 'GET',
      tab: null,
    });
    buttonsArray.push({
      label: 'POST',
      requestType: 'POST',
      tab: null,
    });
    return res.status(200).json(buttonsArray)
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const getGamesUsingGetRequest = async (req, res) => {
  try {
    const url_string = "http://localhost:8082/api/custom-button/?" + req.params.ids;
    const url = new URL(url_string);
    const result = url.searchParams.getAll("id[]");
    const games = await Games.find({ _id: { $in: result } })
    return res.status(200).json(games);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const getGamesUsingPostRequest = async (req, res) => {
  try {
    const { gamesArray } = req.body;
    return res.status(200).json(gamesArray);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

module.exports = { getButtons, getGamesUsingGetRequest, getGamesUsingPostRequest };
