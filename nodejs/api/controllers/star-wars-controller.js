const axios = require('axios');

const getFilms = async (req, res) => {
  try {
    const response = await axios.get('https://swapi.co/api/films');
    res.json(response.data);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const getPlanets = async (req, res) =>{
  try {
    const response = await axios.get('https://swapi.co/api/planets');
    // console.log(response.data);
    res.json(response.data);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const getPeople = async (req, res) =>{
  try {
    const response = await axios.get('https://swapi.co/api/people');
    // console.log(response.data);
    res.json(response.data);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

module.exports = { getFilms, getPlanets, getPeople };
