const { Games } = require('../models/games-model');

const getGames = async (req, res) => {
  try {
    const games = await Games.find({});
    return res.json(games).status(200);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const getOne = async (req, res) => {
  try {
    const id = req.params.id;
    const game = await Games.findById(id);
    return res.json(game).status(200);
  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const addGame = async (req, res) => {
  try {
    const { title, year, rating, genre } = req.body;

    const game = new Games({ title, year, rating, genre });
    console.log(game)
    await game.save();

    res.status(200).json({ message: "Game added" })

  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const deleteGames = async (req, res) => {
  try {
    const { itemsToDelete } = req.body;
    const ids = itemsToDelete.map((item) => item._id);
    await Games.deleteMany({ _id: { $in: ids } });
    res.status(200).json({ message: "Games deleted" })

  } catch (err) {
    return res.json(err.message).status(500);
  }
}

const updateGame = async (req, res) => {
  try {
    const { _id, title, year, rating, genre } = req.body;
    console.log(_id)
    const game = await Games.findById(_id);
    game.title = title;
    game.year = year;
    game.rating = rating;
    game.genre = genre;
    await game.save();
    res.status(200).json(game);

  } catch (err) {
    return res.json(err.message).status(500);
  }
}

module.exports = { getGames, addGame, deleteGames, updateGame, getOne };
