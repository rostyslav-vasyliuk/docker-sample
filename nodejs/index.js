var express = require('express')
var app = express()
const bodyParser = require('body-parser');
const apiRouter = require('./api/routes/api-routes');
const cors = require('cors');
const mongoose = require('mongoose');

mongoose.connect("mongodb://mongoDB:27017/testdb");

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "access-token");
  res.header("Access-Control-Expose-Headers", "access-token");
  next();
});

app.use(cors());


app.use(bodyParser.json({ limit: '5mb' }));

app.use(bodyParser.text({ type: 'text/plain', limit: '5mb' }));

app.use('/api', apiRouter);

app.get('/', (req, res) => res.json('App get works'));

app.listen(8082, () => console.log('app listening on port 8082!'));
