# Installation

### Install using docker-compose

In the root directory run command:

```
docker-compose up --build
```

Your server will run on port http://localhost:8082
MongoDB will be hosted on :27017

If you have no images installed (currently used node 10 and mongo latest) it will be installed automatically.

### Install using Makefile

You need to use:
```
make build
```
